+++
title = 'Test'
date = 2024-02-10T12:54:27-05:00
draft = true
+++
{{< katex />}}

## Introduction

This is a test page

$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$
